import getBrands from "./Brands";
import React from "react";
import logo from "./logo.svg";
import "./App.css";
import getModels from "./Models";
import getYears from "./Years";
import getVersions from "./Versions";
import getCar from "./Cars";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      brands: [],
      models: [],
      years: [],
      versions: [],

      currentBrand: null,
      currentModel: null,
      currentYear: null,
      currentVersion: null
    };
  }

  async componentDidMount() {
    const brands = await getBrands();
    this.setState({ brands: brands });
  }

  async selectBrand(brand) {
    this.setState({ currentBrand: brand });
    const models = await getModels(brand);
    this.setState({ models: models });
  }
  async selectModel(model) {
    this.setState({ currentModel: model });
    const years = await getYears(this.state.currentBrand, model);
    this.setState({ years: years });
  }
  async selectYear(year) {
    this.setState({ currentYear: year });
    const versions = await getVersions(
      this.state.currentBrand,
      this.state.currentModel,
      year
    );
    this.setState({ versions: versions });
  }
  async selectVersion(version) {
    this.setState({ currentVersion: version });
    const car = await getCar(
      this.state.currentBrand,
      this.state.currentModel,
      this.state.currentyear,
      version
    );
    this.setState({ car: car });
  }
  render() {
    return (
      <div className="App">
        <select
          id="Brands"
          onChange={e => {
            this.selectBrand(e.target.value);
          }}
        >
          {this.state.brands.map(brand => (
            <option value={brand}>{brand}</option>
          ))}
        </select>
        <select
          id="Models"
          onChange={e => {
            this.selectModel(e.target.value);
          }}
        >
          {this.state.models.map(model => (
            <option value={model}>{model}</option>
          ))}
        </select>
        <select
          id="Years"
          onChange={e => {
            this.selectYear(e.target.value);
          }}
        >
          {this.state.years.map(year => (
            <option value={year}>{year}</option>
          ))}
        </select>
        <select
          id="Versions"
          onChange={e => {
            this.selectVersion(e.target.value);
          }}
        >
          {this.state.versions.map(version => (
            <option value={version.versionId}>{version.version}</option>
          ))}
        </select>
        <header className="App-header">
          <a href="index.html">
            <img
              src="https://www.volanty.com/assets/img/volantyog.png"
              alt=""
            />
          </a>
          <h1>Volanty/Calindra</h1>
        </header>
      </div>
    );
  }
}

export default App;
