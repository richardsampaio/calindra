import axios from "axios";
function getVersions(brand, model, year) {
  return axios
    .get(
      "https://volanty-price-api.herokuapp.com/brands/" +
        brand +
        "/models/" +
        model +
        "/years/" +
        year +
        "/versions"
    )
    .then(function(response) {
      console.log(response.data);
      return response.data;
    });
}

export default getVersions;
