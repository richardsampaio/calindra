import axios from "axios";
function getBrands() {
  return axios
    .get("https://volanty-price-api.herokuapp.com/brands")
    .then(function(response) {
      console.log(response.data);
      return response.data;
    });
}

export default getBrands;
