import axios from "axios";
function getYears(brand, model) {
  return axios
    .get(
      "https://volanty-price-api.herokuapp.com/brands/" +
        brand +
        "/models/" +
        model +
        "/years"
    )
    .then(function(response) {
      console.log(response.data);
      return response.data;
    });
}

export default getYears;
