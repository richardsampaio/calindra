import axios from "axios";
function getModels(brand) {
  return axios
    .get("https://volanty-price-api.herokuapp.com/brands/" + brand + "/models")
    .then(function(response) {
      return response.data;
    });
}

export default getModels;
