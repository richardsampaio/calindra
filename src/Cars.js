import axios from "axios";
function getCar(brand, model, year, versionId) {
  return axios
    .get(
      "https://volanty-price-api.herokuapp.com/brands/" +
        brand +
        "/models/" +
        model +
        "/years/" +
        year +
        "/versions/" +
        versionId
    )
    .then(function(response) {
      console.log(response.data);
      return response.data;
    });
}

export default getCar;
